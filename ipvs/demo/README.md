# **Demo**

### **Requirements:**
```bash
sudo apt install ipvsadm docker.io docker-compose jq
```

### **Testing:**
```sh
sudo ./run.sh [ipvsAddr] [nbTorInstances]

ipvsAddr            Virtual IP Address (default: 1.2.3.4)
nbTorInstances      Number of tor instances (default: 3)
```

### **Cleaning**
```sh
ipvsadm -C
docker-compose stop
```