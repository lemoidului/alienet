#!/bin/bash

usage() {
	echo "This script must be run as root"
	echo -e "\nUsage: $0 ipvsAddress nbTorInstance\n"
}

# Show help
if [[ "$*" == "--help" || "$*" == "-h" ]] 
then
	usage
	exit 0
fi

# Check if user is root
if [[ "$EUID" -ne 0 ]]
then
	echo "This script must be run as root"
	exit 1
fi

# Check if $1 is a valid ip address
if [[ ! -z $1 ]]
then
	regexp='([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])'

	if [[ "$1" =~ ^$regexp\.$regexp\.$regexp\.$regexp$ ]]
	then
		# Create ipvs addr TCP , UDP and round robin mode
		echo "Using $1"
		ipvsadm -A -t $1:9050 -s rr
		ipvsadm -A -u $1:9050 -s rr
	else
		echo "ipvs address is not valid: $1"
		echo "Using 1.2.3.4"
		ipvsadm -A -t 1.2.3.4:9050 -s rr
		ipvsadm -A -u 1.2.3.4:9050 -s rr

		# Assign default value to $1
		set -- "1.2.3.4" "${@:2}"	
	fi
else
	echo "Using 1.2.3.4"
	ipvsadm -A -t 1.2.3.4:9050 -s rr
	ipvsadm -A -u 1.2.3.4:9050 -s rr

	# Assign default value to $1
	set -- "1.2.3.4" "${@:2}"	
fi


# Run tor containers instance
if [[ -z "$2" ]] 
then
	echo -e "Running 3 tor instances\n"
	docker-compose up -d --scale tor=3
else
	echo -e "Running $2 tor instances\n"
	docker-compose up -d --scale tor="$2"
fi


# Get containers ip address
ips=$(docker network inspect net_tor | jq -r 'map(.Containers[].IPv4Address) []' | rev | cut -c4- | rev)

# Add containers ip to ipvs
for ip in $(echo "$ips")
do
	#echo "$ip"
	ipvsadm -a -t $1:9050 -r "$ip":9050 -m
	ipvsadm -a -u $1:9050 -r "$ip":9050 -m
done

echo -e "\n"
ipvsadm -l 
echo -e "\nTry it with \n\tcurl -x socks5://$1:9050 https://checkip.amazonaws.com/"
echo -e "\nDon't forget to delete ipvs address when you've finished to play with"
echo -e "\tipvsadm -C"
echo -e "\tdocker-compose stop"
