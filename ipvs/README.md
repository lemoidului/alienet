# Play with IPVS and deploy a TOR L4 loadbalancing SOCKS(v5) cluster


### What is IPVS?
*IPVS* is a kernel feature providing layer 4 load balancing. It is also called Layer 4 switching. A stable version of IPVS is available since Linux 2.6.  
In previous versions of Kubernetes, services (managed by kube-proxy) were implemented with IPTables rules. The IPVS feature is intended to replace this mechanism: instead of setting new iptables rules, kube-proxy can now use the IPVS mechanism to implement the services.  
### Don't care about k8s and the spaghetti network layer they are using !!!
By the way, k8s is not the only reason to give some interest in IPVS. You can use it also with other scenarios like here, a Tor cluster.  
I've seen some implementations for such TOR L4 Loadbalancing with HAP or Traeffik in front. This one with IPVS is fare more efficient and much easier to deploy.  
Once ipvasdm is installed, read the man for a complete ovierview of this feature and of course you can adapt all of this with a script and / or docker-compose to fit with your usage.  


```
                        +--------------------------------+    
             /----------|        Tor1 172.17.0.2:9050    |------->    
            |           +--------------------------------+    
        +------------+  +--------------------------------+    
------->|1.2.3.4:9050|--|        Tor2 172.17.0.3:9050    |------->    
        +------------+  +--------------------------------+    
            |           +--------------------------------+    
             \----------|        Tor3 127.17.0.4:9050    |------->
                        +--------------------------------+

```


```bash
# first install ipvsadm
sudo apt install ipvsadm

# then launch containers 
docker run --rm -d --name tor1 osminogin/tor-simple
docker run --rm -d --name tor2 osminogin/tor-simple
docker run --rm -d --name tor3 osminogin/tor-simple

# check containers address
# Obtain 172.17.0.2, 172.17.0.3, 172.17.0.4 if no more containers are in use
docker inspect --format '{{ .NetworkSettings.IPAddress }}' tor1
docker inspect --format '{{ .NetworkSettings.IPAddress }}' tor2
docker inspect --format '{{ .NetworkSettings.IPAddress }}' tor3


# create ipvs addr, TCP and round robin mode (other modes are availables)
ipvsadm -A -t 1.2.3.4:9050 -s rr

# Add containers address to IPVS address
ipvsadm -a -t 1.2.3.4:9050 -r 172.17.0.2:9050 -m
ipvsadm -a -t 1.2.3.4:9050 -r 172.17.0.3:9050 -m
ipvsadm -a -t 1.2.3.4:9050 -r 172.17.0.4:9050 -m

# check by using the socks proxy
curl -x socks5://1.2.3.4:9050 http://checkip.amazonaws.com/
X.X.X.X
curl -x socks5://1.2.3.4:9050 http://checkip.amazonaws.com/
Y.Y.Y.Y
curl -x socks5://1.2.3.4:9050 http://checkip.amazonaws.com/
Z.Z.Z.Z
```
