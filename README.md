# Alienet

Mostly uncommon Linux Network configuration & network labs tests

 - [6rd](../6rd):  A complete 6rd router gateway configuration based on the new nftable implementation. This conf is use for a french ISP (Free/Iliad SAS) 

 - [ipvs](../ipvs): Fast Tor L4 Loadbalancer build with IPVS

 - [w2n](../w2n): Send wireguard Tunnel in another namespace for fun and profit 
